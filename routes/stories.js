const express = require('express');
const router = express.Router();
const authHelper = require("../helpers/auth");
const db         = require("../models");
const mongoose   = require("mongoose");

/* GET all posts / stories. */
router.get('/', (req, res)=> {
  db.Story.find({status: "public"})
  .populate("user")
  .sort({date: "desc"})
  .then(stories=>{
    res.render('stories/index',{stories: stories});
    })
    .catch(error=>{
      res.send(error);
    })
  });
  /* GET single posts / stories. */
  router.get("/show/:id", (req, res)=>{
    db.Story.findById(req.params.id)
    .populate("user")
    .populate("comments.commentUser")
    .then(story=>{
      res.render("stories/show", {story: story});
    })
    .catch(error=>{
      res.send(error);
      console.log(error);
    })
  });
  //list stories from a specific user
  router.get("/user/:userId", (req, res)=>{
    db.Story.find({user: req.params.userId, status: "public"})
    .populate("user")
    .then(stories=>{
      res.render("stories/index", {stories: stories});
    })
    .catch(error=>{
      res.send(error);
      console.log(error);
    })
  });
  //list of all my posts/stories
  router.get("/my", authHelper.isLoggedin,(req, res)=>{
    db.Story.find({user: req.user.id})
    .populate("user")
    .populate("comments.commentUser")
    .then(stories=>{
      res.render("stories/index", {stories: stories});
    })
    .catch(error=>{
      res.send(error);
      console.log(error);
    })
  });
/* Add post form*/
router.get('/add',authHelper.isLoggedin ,(req, res)=> {
  res.render('stories/add');
});
/* Edit post form*/
router.get('/edit/:id',authHelper.isOwner ,(req, res)=> {
  db.Story.findById(req.params.id)
    .populate("user")
    .then(story=>{
        res.render('stories/edit', {story: story});   
    })
    .catch(error=>{
      res.send(error);
      console.log(error);
    })
});

/* POST ROUTES */
/* POST Story */
router.post("/", (req, res)=>{
  let allowComments;
  if(req.body.allowComments){
    allowComments = true;
  }else{
    allowComments = false;
  }
  let newStory = {
    title: req.body.title,
    image: req.body.image,
    body: req.body.body,
    status: req.body.status,
    allowComments: allowComments,
    user: req.user.id
  }
  db.Story.create(newStory)
  .then(story=>{
    res.redirect(`/stories/show/${story._id}`);  
    console.log(story);  
  })
  .catch(err=>{
      res.send(error);
  });
  
});
/* Edit Story */
router.put("/:id",authHelper.isOwner ,(req, res)=>{
  let allowComments;
  if(req.body.allowComments){
    allowComments = true;
  }else{
    allowComments = false;
  }
  let newStory = {
    title: req.body.title,
    image: req.body.image,
    body: req.body.body,
    status: req.body.status,
    allowComments: allowComments,
    user: req.user.id
  }
  db.Story.findByIdAndUpdate(req.params.id, newStory)
  .then(story=>{
    req.flash("success_msg", `${story.title} has been edited successfully!`);
    res.redirect("/dashboard");
  })
  .catch(error=>{
    res.send(error);
    console.log(error);
  });
});
//delete story
router.delete("/:id", authHelper.isOwner,(req, res)=>{
  db.Story.findByIdAndRemove(req.params.id)
  .then(()=>{
    res.redirect("/dashboard");
  })
  .catch(error=>{
    res.send(error);
    console.log(error);
  });
});

//comments routes
//add route
router.post("/comment/:id", (req, res)=>{
  db.Story.findById(req.params.id)
  .then(story=>{
    let newComment = {
      commentBody: req.body.commentBody,
      commentUser: req.user.id
    }
    //push to comments array
    story.comments.unshift(newComment);
    story.save()
    .then(story=>{
      res.redirect(`/stories/show/${story.id}`);
    })
    .catch(error=>{
      res.send(error);
      console.log(error);
    });
  })
  .catch(error=>{
    res.send(error);
    console.log(error);
  });
});

module.exports = router;