const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get("/google", passport.authenticate('google', {scope: [
  'profile','email']}));

  router.get( '/google/callback', 
    passport.authenticate( 'google', { 
        successRedirect: '/dashboard',
        failureRedirect: '/'
}));
router.get("/verify", (req, res)=>{
  if (req.user) {
    console.log(`user is logged in, here: ${req.user}`);
    
  } else {
    console.log(`not logged in`);
  }
});
router.get("/logout", (req, res)=>{
  req.logout();
  res.redirect("/");
});


module.exports = router;