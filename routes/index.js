const express = require('express');
const router = express.Router();
const authHelper = require("../helpers/auth");
const db         = require("../models");

/* GET home page. */
router.get('/', authHelper.isGuest ,function(req, res, next) {
  res.render('index/welcome', {layout: 'landing.handlebars'});
});
router.get('/about', function(req, res, next) {
  res.render('index/about');
});
//dashboard route

router.get('/dashboard', authHelper.isLoggedin,(req, res)=> {
  db.Story.find({user: req.user.id})
  .then(stories=>{
    res.render("index/dashboard", {stories: stories});
  })
  .catch(error=>{
    res.send(error);
    console.log(error);
  });
  
});
module.exports = router;
