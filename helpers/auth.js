const db = require("../models");

let middlewareObj = {};
//checking if user logged in
middlewareObj.isLoggedin = (req, res, next)=>{
  if (req.isAuthenticated()) {
      return next();
  } else {

      res.redirect(www.google.com);
  }
}
middlewareObj.isGuest = (req, res, next)=>{
  if (req.isAuthenticated()) {
     res.redirect("/dashboard"); 
  } else {
    return next();
  }
}
middlewareObj.isOwner = (req, res, next)=>{
  db.Story.findById(req.params.id)
  .then(story=>{
    if (req.user.id == story.user) {
            return next();
    } else {
      res.redirect("/");
      console.log("You're not the owner of this post");
    }
  })
  .catch(error=>{
    res.send(error);
    console.log(error);
  });
  
}

module.exports = middlewareObj;