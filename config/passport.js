const GoogleStrategy = require('passport-google-oauth20').Strategy,
      mongoose       = require('mongoose'),
      db             = require('../models'),
      keys           = require('./keys');

module.exports =  (passport)=>{
  passport.use(
    new GoogleStrategy({
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: "/auth/google/callback",
      proxy      :  true
    }, (req, accessToken, refreshToken, profile, done)=>{
          // console.log(accessToken);
          // console.log(profile);
          const image = 
          profile.photos[0].value.substring(0, profile.photos[0].value.indexOf("?"));
          const newUser = {
            googleID: profile.id,
            email: profile.emails[0].value,
            firstName: profile.name.givenName,
            lastName:profile.name.familyName,
            image: image
          }

          //checking if user already exist
          db.User.findOne({googleID: profile.id})
          .then(user=>{
            user.image= profile.photos[0].value.substring(0, profile.photos[0].value.indexOf("?"));
            user.firstName = profile.name.givenName;
            user.lastName = profile.name.familyName;
            user.save();
            if (user) {
              //return user
              console.log(`user is found: ${user}`);
              done(null, user);
              
            } else {
              //create user
              new db.User(newUser)
              .save()
              .then(user => done(null, user))
              .catch(err=>{
                console.log(err);
              });
            }
          });
    }));
  passport.serializeUser((user, done)=>{
    done(null, user.id);
  });
  passport.deserializeUser((id, done)=>{
    db.User.findById(id)
    .then(user=>{
      
      done(null, user);
    })
    .catch(err=>{
      console.log(err);
    })
  })

}