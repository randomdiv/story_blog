const mongoose = require("mongoose");
const keys     = require('../config/keys');
mongoose.set("debug", true);
//connect to database
mongoose.Promise = global.Promise;
mongoose.connect(keys.mongoURI)
  .then(() => {
    console.log("Database is connected");
  })
  .catch(err => {
    console.log(err);
  });

  module.exports.User = require("./user");
  module.exports.Story = require("./story");