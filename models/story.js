const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var storySchema = new Schema({
    title: {
        type: String
    },
    image: {
        type: String
    },
    body: {
        type: String
    },
    status: {
        type: String,
        default: "public"
    },
    date: {
        type: Date,
        default: Date.now
    },
    allowComments:{
        type: Boolean,
        default: true
    },
    comments:[{
        commentBody: {
            type: String,
            required: true
        },
        commentDate: {
            type: Date,
        default: Date.now
        },
        commentUser:{
            type: Schema.Types.ObjectId,
            ref: "User"
        }
    }],
    user:{
        type: Schema.Types.ObjectId,
        ref: "User"
    }
});
var Story = mongoose.model("Story", storySchema, "stories");

module.exports = Story;