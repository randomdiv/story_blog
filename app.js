const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const session      = require('express-session');
const bodyParser = require('body-parser');
const mongoose   = require('mongoose');
const passport   = require('passport');
const db         = require("./models");
const exphbs  = require('express-handlebars');
const methodOverride = require('method-override');
const flash       = require("connect-flash");

//passport config
require('./config/passport')(passport);

//routes
const index = require('./routes/index');
const users = require('./routes/users');
const auth  = require('./routes/auth');
const stories  = require('./routes/stories');
// handlebars helpers
const {
  truncate,
  stripTags,
  formatDate,
  select,
  alertsuccess,
  alertErr,
  editIcon
} = require("./helpers/hbs");
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'hbs');
app.engine('handlebars', exphbs({
  helpers: {
    truncate: truncate,
    stripTags: stripTags,
    formatDate: formatDate,
    select: select,
    alertErr: alertErr,
    alertsuccess: alertsuccess,
    editIcon: editIcon
  },
  defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
//body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// method override middleware
app.use(methodOverride('_method'));
//cookie parser
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//express middleware
app.use(session({
  secret: 'Elsa is a loveable dog but ever So Annoying 900192018hjsadkjh2_@',
  resave: false,
  saveUninitialized: false
}));
app.use(flash());
//passport middleware
app.use(passport.initialize());
app.use(passport.session());
//set global variables
app.use((req, res, next)=>{
  res.locals.user = req.user || null;
  res.locals.error_msg = req.flash("error");
  res.locals.success_msg = req.flash("success");
  next();
});
//using routes
app.use('/', index);
app.use('/users', users);
app.use('/auth', auth);
app.use('/stories', stories);

const port = process.env.PORT || 5000;
app.listen(port, ()=>{
  console.log(`app started on port: ${port}`);
})

module.exports = app;
